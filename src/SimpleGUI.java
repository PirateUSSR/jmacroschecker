import com.sun.deploy.panel.RadioPropertyGroup;


import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleGUI extends JFrame {

    private JButton button = new JButton("���������");
    private JTextField input1 = new JTextField("", 1);
    private JTextField input2 = new JTextField("", 1);
    private JTextField input3 = new JTextField("", 1);
    private JTextField input4 = new JTextField("", 1);
    private JTextField input5 = new JTextField("", 1);
    private JLabel label1 = new JLabel("������� ����� ��������� ���������� � ������������ �������: ");
    private JLabel label2 = new JLabel("������� ����� ��������� ���������� � �������������� �������: ");
    private JLabel label3 = new JLabel("������� ����� ���������� ������(������ N:\\): ");
    private JLabel label4 = new JLabel("������� ����� ���������� ��� �������: ");
    //    private JCheckBox check1 = new JCheckBox("�������� ����� �� ������� N: �� ����������� ", false);
    private JRadioButton changeToSigned = new JRadioButton("�������� ����� �� ������� N �� ����������� � ������� ������: ");
    private JRadioButton JustTakeReports = new JRadioButton("������ ����������������  � ������������ ������: ");
    private JRadioButton returnChanges = new JRadioButton("�������� ������ �� ����������� �����: ");
    private JRadioButton removeAllBackupFiles = new JRadioButton("������� ��� \"backup\" ����� � ������� N:");

    private JLabel label5 = new JLabel("�� ����� �� ����������� �����?: ");
    private JLabel clearlabel1 = new JLabel("");
    private JLabel statusBar = new JLabel("");

    private JCheckBox useSignetEtalon = new JCheckBox("�� ������ N:\\ ��� �������� ����� �� ����������� � ������� ������: ");
    private JCheckBox createPackages = new JCheckBox("");

    public SimpleGUI() {
        super("�������� ��������");
        this.setBounds(100, 100, 1000, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(9, 2, 2, 2));
        container.add(label1);
        container.add(input1);
        container.add(label2);
        container.add(input2);
        container.add(label3);
        container.add(input3);
        container.add(label4);
        container.add(input4);
        container.add(label5);
        container.add(input5);


        ButtonGroup rbgroup1 = new ButtonGroup();
        rbgroup1.add(changeToSigned);
        rbgroup1.add(removeAllBackupFiles);
        rbgroup1.add(JustTakeReports);

        container.add(JustTakeReports);
        JustTakeReports.setSelected(true);
        container.add(clearlabel1).setVisible(false);
        rbgroup1.add(returnChanges);
        container.add(changeToSigned);

        container.add(statusBar).setVisible(false);
        container.add(returnChanges);
        container.add(removeAllBackupFiles);
//        container.add(useSignetEtalon);

//        container.add(createPackages);
        createPackages.setSelected(false);


        ButtonGroup group = new ButtonGroup();

        if (removeAllBackupFiles.isSelected()) {
            button.setText("������� ��� ������");
            container.add(button);
        }

        if (returnChanges.isSelected())
            button.setText("�������� ���������");
        if (changeToSigned.isSelected())
            button.setText("�������� �����");

        button.addActionListener(new ButtonEventListener());
        container.add(button);


    }

    class ButtonEventListener implements ActionListener {

        public boolean checkData(String UnsignedEtalonDir, String SignedEtalonDir, String findDir, String reportPath) {
            boolean check = false;
            if (UnsignedEtalonDir.length() <= 0 || SignedEtalonDir.length() <= 0 || findDir.length() <= 0 || reportPath.length() <= 0) {
                check = false;
            } else {
                File UNSIGNED_ETALON_PATH = new java.io.File(UnsignedEtalonDir);
                File SIGNED_ETALON_PATH = new java.io.File(SignedEtalonDir);
                File Find_PATH = new java.io.File(findDir);
                File REPORT_PATH = new java.io.File(reportPath);

                if (UNSIGNED_ETALON_PATH.isDirectory() && Find_PATH.isDirectory() && REPORT_PATH.isDirectory() && SIGNED_ETALON_PATH.isDirectory()) {
                    check = true;
                }
            }
            return check;
        }

        public void actionPerformed(ActionEvent e) {
            Date startTime = new Date();
            SimpleDateFormat formatStartTime = new SimpleDateFormat("hh:mm");

            long start = System.currentTimeMillis();


            String message = "";
            String debug = "";
            Boolean Errors = false;
            String errorMsg = "";
            int newDialogResult = 0;
            String SignedEtalonDir = null;
            String UnsignedEtalonDir = null;

            String findDir = null;

            String reportPath = null;

            String AS = null;


            SignedEtalonDir = input1.getText();
            UnsignedEtalonDir = input2.getText();

            findDir = input3.getText();

            reportPath = input4.getText();

            AS = input5.getText();

            String osType = System.getProperty("os.name");
            boolean debugMode = true;

            if (debugMode) {
                if (osType.equals("Linux")) {

                    //Linux default Paths for Debuging
                    UnsignedEtalonDir = "/mnt/Disk/MyCode/jmacroschecker/Data/NewTestFolder/UnSigned";

                    SignedEtalonDir = "/mnt/Disk/MyCode/jmacroschecker/Data/NewTestFolder/Signed";

                    findDir = "/mnt/Disk/MyCode/jmacroschecker/Data/NewTestFolder/N";

                    reportPath = "/mnt/Disk/MyCode/jmacroschecker/Data/Reports";
                    AS = "���";
                } else {

                    //Windows default Paths for Debuging

                    SignedEtalonDir = "D:\\MyCode\\jmacroschecker\\Data\\NewTestFolder\\Signed";
                    UnsignedEtalonDir = "D:\\MyCode\\jmacroschecker\\Data\\NewTestFolder\\UnSigned";
                    //fake paths

                    findDir = "D:\\MyCode\\jmacroschecker\\Data\\NewTestFolder\\N";

                    reportPath = "D:\\MyCode\\jmacroschecker\\Data\\Reports";
                    AS = "���";
                }
            }


//            if (useSignetEtalon.isSelected())
//                UnsignedEtalonDir = SignedEtalonDir;

            if (removeAllBackupFiles.isSelected()) {
                if (checkData(UnsignedEtalonDir, SignedEtalonDir, findDir, reportPath)) {

                    String warningMessage = "��������!!! � ������� N:\\ ����� ������������ ������� ��� " +
                            "\"backup\" �����! ������� ����������? ";
                    int dialogButton = JOptionPane.YES_NO_OPTION;
                    int dialogResult = JOptionPane.showConfirmDialog(null, warningMessage, "Warning", dialogButton);
                    if (dialogResult != 0) {
                        removeAllBackupFiles.setSelected(false);
                        JustTakeReports.setSelected(true);
                        int newDialogButton = JOptionPane.YES_NO_OPTION;
                        newDialogResult = JOptionPane.showConfirmDialog(null, "�������� \"backup\" ������ ��������!\n" +
                                "���������� ������� ������ � �������� ������?", "Outpoot", newDialogButton);
                    }
                }

            }

            if (newDialogResult == 0) {

                if (!checkData(UnsignedEtalonDir, SignedEtalonDir, findDir, reportPath) && !removeAllBackupFiles.isSelected()) {
                    message += "������������ ������ ��� �������!\n��������� ������������ ����� � ��������� �������!";
                } else {
                    File UNSIGNED_ETALON_PATH = new java.io.File(UnsignedEtalonDir);
                    File SIGNED_ETALON_PATH = new java.io.File(SignedEtalonDir);
                    File Find_PATH = new java.io.File(findDir);
                    File REPORT_PATH = new java.io.File(reportPath);

                    if (UNSIGNED_ETALON_PATH.isDirectory() && Find_PATH.isDirectory() && REPORT_PATH.isDirectory() && SIGNED_ETALON_PATH.isDirectory()) {

                        ProgramConfig config = new ProgramConfig(statusBar, changeToSigned.isSelected(), JustTakeReports.isSelected(), returnChanges.isSelected(), removeAllBackupFiles.isSelected(), createPackages.isSelected(), SignedEtalonDir, AS, osType);
                        LogicFunctions logic = new LogicFunctions(config);
                        logic.removeOldReports(reportPath);//Function for removing old reports

                        String mainPathDir = null;
                        File MainClassPath = null;
                        try {
                            MainClassPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
                        } catch (URISyntaxException e1) {
                            e1.printStackTrace();
                        }

//                        try{
//                            decodedPath = URLDecoder.decode(path, "UTF-8");
//                        }catch (Exception decode){
//                            decode.printStackTrace();
//                        }
                        mainPathDir = MainClassPath.toString();

//                        JOptionPane.showMessageDialog(null, mainPathDir, "decodedPath", JOptionPane.PLAIN_MESSAGE);

                        String[] parts = mainPathDir.split("\\\\");

                        StringBuilder strBuilder = new StringBuilder();

                        for (int i = 0; i < parts.length - 1; i++) {
                            strBuilder.append(parts[i]);
                            strBuilder.append("\\");
                        }
                        mainPathDir = strBuilder.toString();

                        config.mainClassPath = mainPathDir;
//                        JOptionPane.showMessageDialog(null, config.mainClassPath, "decodedPath", JOptionPane.PLAIN_MESSAGE);

                        config.auxiliaryFilesPath = config.mainClassPath + "AuxiliaryFiles";
                        config.fileListForReplace = config.mainClassPath + AS + "_filesListForReplace.txt";

                        File fileListForReplace = new File(config.fileListForReplace);
                        if (fileListForReplace.exists() && config.justTakeReport) {
                            fileListForReplace.delete();
                        }

                        File errorLogFile = new File(config.mainClassPath + "ErrorLog.txt");
                        if (errorLogFile.exists())
                            errorLogFile.delete();


                        File file = new File(config.fileListForReplace);

                        if (config.returnChanges) {
                            try {
                                System.out.println("test debuging");
                            } catch (Exception e3) {
                                e3.getCause();
                            }
                            try {
                                FileInputStream fstream = new FileInputStream(config.fileListForReplace);
                                BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
                                String strLine;
                                int count = 0;
                                File fileSource = null;
                                File fileDest = null;
                                File backupFile = null;
                                while ((strLine = br.readLine()) != null) {
                                    count++;

                                    if (count == 2) {
//                                        System.out.println(strLine);
                                        fileSource = new File(strLine);
                                    }
                                    if (count == 3) {
                                        System.out.println(strLine);
                                        fileDest = new File(strLine);
                                        backupFile = new File(strLine + ".backup");
                                    }


                                    if (count == 4) {
                                        try {
                                            logic.copyFileUsingStream(backupFile, fileDest, reportPath);
                                            backupFile.delete();
                                        } catch (Exception e2) {
                                            e2.getCause();
                                        }
                                        count = 0;
                                    }


                                }
                            } catch (IOException e1) {
                                System.out.println("������");
                            }
                        } else {
                            if (config.replaceFile) {
                                File existFile = new File(config.fileListForReplace);
                                if (existFile.exists()) {
                                    File copyFilelog = new File(config.mainClassPath + "copyLog.txt");
                                    if (copyFilelog.exists())
                                        copyFilelog.delete();
                                    try {
                                        FileInputStream fstream = new FileInputStream(config.fileListForReplace);
                                        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
                                        String strLine;
                                        int count = 0;
                                        File fileSource = null;
                                        File fileDest = null;
                                        File backupFile = null;
                                        while ((strLine = br.readLine()) != null) {
                                            count++;

                                            if (count == 2) {
//                                        System.out.println(strLine);
                                                fileSource = new File(strLine);
                                            }
                                            if (count == 3) {
                                                System.out.println(strLine);
                                                fileDest = new File(strLine);
                                                backupFile = new File(strLine + ".backup");
                                                logic.copyFileUsingStream(fileDest, backupFile, reportPath);
                                            }


                                            if (count == 4) {
                                                try {
                                                    logic.copyFileUsingStream(fileSource, fileDest, reportPath);
                                                    logic.appendUsingFileWriter(config.mainClassPath + "copyLog.txt", "������� ����: " + fileDest + "\r\n");
                                                } catch (Exception e2) {
                                                    e2.getCause();
                                                }
                                                count = 0;
                                            }


                                        }
                                    } catch (IOException e1) {
                                        System.out.println("������");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "������!!! ���� " + config.fileListForReplace + " �� ������!", "Error!!!", JOptionPane.PLAIN_MESSAGE);
                                    Errors = true;
                                    errorMsg = "���� " + config.fileListForReplace + " �� ������!";
                                }

                            } else {
                                logic.FileCmp(UNSIGNED_ETALON_PATH, Find_PATH, reportPath, config, 0, 0);
                            }
                        }


//                        JOptionPane.showMessageDialog(null,
//                                config.mainClassPath+"\n"+config.mainClassPath + "AuxiliaryFiles"+"\n"+config.debug, "Outpoot", JOptionPane.PLAIN_MESSAGE);
//                        JOptionPane.showMessageDialog(null,config.fileListForReplace+config.systemNameToReplace+
//                                config.fileToReplaceFromPaths+config.fileToReplaceToPaths+"n/a",
//                                "Outpoot", JOptionPane.PLAIN_MESSAGE);


//                        try{
//                            logic.writeXLSXReplaceFile(config.fileListForReplace, config.systemNameToReplace,
//                                    config.fileToReplaceFromPaths, config.fileToReplaceToPaths, "n/a");
//                        }catch (Exception n){
//                            n.getCause();
//                        }


//                        for (int i = 0; i < config.systemNameToReplace.size(); i++){
//                            logic.appendUsingFileWriter(config.fileListForReplace, config.systemNameToReplace.get(i)+"\r\n");
//                            logic.appendUsingFileWriter(config.fileListForReplace, config.fileToReplaceFromPaths.get(i)+"\r\n");
//                            logic.appendUsingFileWriter(config.fileListForReplace, config.fileToReplaceToPaths.get(i)+"\r\n");
//                            logic.appendUsingFileWriter(config.fileListForReplace, "n/a"+"\r\n");
//                        }

//                        try{
//                            logic.writeXLSXReplaceFile("D:\\MyCode\\jmacroschecker\\Data\\Reports\\test.xlsx", config.systemNameToReplace,
//                                    config.fileToReplaceFromPaths, config.fileToReplaceToPaths,"n/a");
//                        }catch (Exception n){
//                            n.getCause();
//                        }


                        if (Errors) {
                            message += "��������� ����������� � �������!\n����� � ������� ���������� �� ������: " + config.mainClassPath + "ErrorLog.txt";
                            logic.appendUsingFileWriter(config.mainClassPath + "ErrorLog.txt", errorMsg + "\r\n");
                        } else {
                            if (JustTakeReports.isSelected()) {


                                message += "��������� ���������!\n";
                            }

                            if (changeToSigned.isSelected()) {

                                //Changing files function TO WRITE
                                message += "��� ����� �������� �� �����������!\n������� ������ ������������� ������!\n";
                            }


                            if (removeAllBackupFiles.isSelected())
                                message += "��� backup ����� �������!\n";


                            if (returnChanges.isSelected())

                                message += "���������� ����� ������ ������!\n";
                            if (createPackages.isSelected()) {
                                message += "������ �� ������� ������������ �� ������: " +
                                        reportPath + "\n";
                            }
                            message += "������ ������������ �� ������: " + reportPath + "\n";
                        }
                    } else {
                        message += "��������� ������ �����������!\n��������� ������������ ����� � ��������� �������!";
                    }

                    Date finishTime = new Date();
                    SimpleDateFormat formatFinishTime = new SimpleDateFormat("hh:mm");

                    long finish = System.currentTimeMillis();
                    long timeConsumedMillis = finish - start;

                    message += "\n����� ���������� � : " + formatStartTime.format(startTime);
                    message += "\n��������� ���������� � : " + formatFinishTime.format(finishTime);


                    message += "\n����� ������ ���������� � �������: " + timeConsumedMillis / 60000;
                }

                JustTakeReports.setSelected(true);

                JOptionPane.showMessageDialog(null, message, "Outpoot", JOptionPane.PLAIN_MESSAGE);

            } else {
                JOptionPane.showMessageDialog(null, "��������� � �������!", "Outpoot", JOptionPane.PLAIN_MESSAGE);

            }

        }


    }

}

