import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ProgramConfig {
    public boolean replaceFile = false;
    public boolean createBackupOnN = false;
    public String signedPath = null;
    public String AS = "";
    public String re = "";
    public boolean fileAnalyzed = false;
    public boolean justTakeReport = false;
    public boolean returnChanges = false;
    public JLabel statusBar = null;
    public boolean createPackages = false;
    public boolean removeAllBackups = false;
    public List<String> fileToReplaceFromPaths = new ArrayList();
    public List<String> fileToReplaceToPaths = new ArrayList();
    public List<String> systemNameToReplace = new ArrayList<String>();
    public String fileListForReplace = null;
    public String mainClassPath = null;
    public String auxiliaryFilesPath = null;
    public String debug = "";

    public ProgramConfig(JLabel statusBar, boolean replaceFile, boolean justTakeReports, boolean returnChanges,boolean removeAllBackups, boolean createPackages, String signedPath, String AS, String osType){
        this.replaceFile = replaceFile;
        this.createBackupOnN = justTakeReports;
        this.signedPath = signedPath;
        this.AS = AS;
        this.removeAllBackups = removeAllBackups;
        this.createPackages = createPackages;
        this.justTakeReport = justTakeReports;
        this.returnChanges = returnChanges;
        this.statusBar = statusBar;

        System.out.println(osType);
        if (osType.equals("Linux"))
            this.re = "/";
        else
            this.re = "\\";
    }

}
