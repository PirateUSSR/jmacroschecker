import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.hssf.usermodel.*;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;


public class LogicFunctions {
    Date dateNow = new Date();
    SimpleDateFormat formatForDateNow = new SimpleDateFormat("MM-dd-yyyy");
    ProgramConfig config = null;

    public LogicFunctions(ProgramConfig config) {
        this.config = config;
    }

    public void FileCmp(File pathFrom, File pathWith, String reportPath, ProgramConfig config, int analizedFiles, int filesCol) {
        File[] fileListFrom;
        fileListFrom = pathFrom.listFiles();

//        //System.out.println(fileListFrom.length);
        String numberOfFiles = Integer.toString(fileListFrom.length - 1 + filesCol);

        for (int i = 0; i < fileListFrom.length; i++) {
//            config.statusBar.setVisible(true);
//            String status = Integer.toString(i + analizedFiles + 1);
//            config.statusBar.setText("���������� " + status + " ������ �� " + numberOfFiles);
            boolean existFile = false;

            config.fileAnalyzed = false;

            if (fileListFrom[i].isDirectory()) {
                File ETALON_PATH_tmp = new java.io.File(fileListFrom[i].getPath());
                FileCmp(ETALON_PATH_tmp, pathWith, reportPath, config, i, fileListFrom.length);
//                    //System.out.println(fileListFrom[i].getPath());
            } else {
                existFile = fileInPath(fileListFrom[i], pathWith, reportPath, existFile);
                if (!existFile) {
                    String text = "\n���������� � ������ \"" + fileListFrom[i] + "\" � ���������� " + pathWith + " �� �������!!!\r\n";
//                        //System.out.println("\n���������� � ������ \"" + fileListFrom[i] + "\" � ���������� "+ pathWith+" �� �������!!!\r\n");
                    appendUsingFileWriter(reportPath + config.re + config.AS + " �����(���������� �� �������)" + formatForDateNow.format(dateNow) + ".txt", text);
                }
            }

        }

    }

    public boolean fileInPath(File file, File path, String reportPath, boolean existFile) {
        File[] fileListWith;
        fileListWith = path.listFiles();
        String fileAnalizeStatus = null;

//        //System.out.println(fileListWith.length);

        for (int i = 0; i < fileListWith.length; i++) {

            if (fileListWith[i].isDirectory()) {  //���� � �������� ������� ����������
                String tempPath = fileListWith[i].toString();
                File NEW_Find_PATH = new java.io.File(tempPath);
                existFile = fileInPath(file, NEW_Find_PATH, reportPath, existFile);
            } else { //���� fileListWith[i] �� ����������
                if (fileListWith[i].isFile()) {
//                    //System.out.println(fileListWith[i]+" is file");

                    //���� ����� ������ ���������
                    if (file.getName().equals(fileListWith[i].getName())) {
                        //�������� �� ������������ ������ ���������
                        if (config.returnChanges) {

                            File backupFile = new java.io.File(fileListWith[i].getParent().toString() + config.re + fileListWith[i].getName()+".backup");

//                            //System.out.println("!!! "+fileListWith[i].getParent().toString() + config.re+"backup." + fileListWith[i].getName());
                            String nameForBackupFile = fileListWith[i].toString();

                            if (backupFile.exists()) {
                                fileListWith[i].delete(); // Removing signed file to return backup
                                //System.out.println("Reterned BACKUP: "+nameForBackupFile);
                                backupFile.renameTo(new java.io.File(nameForBackupFile));
                            } else {
                                //System.out.println("NO BACKUP of file: " + fileListWith[i].getPath());
                            }
                        }

                        if (config.removeAllBackups) {

                            File backupFile = new java.io.File(fileListWith[i].getParent().toString() + config.re + fileListWith[i].getName()+".backup");

                            if (backupFile.exists())
                                backupFile.delete();
                            else
                                System.out.println("BACKUP FILE not EXIST");
                        }
                        else{
                            if (file.length() == fileListWith[i].length() && file.getName().equals(fileListWith[i].getName())) {// ���� ���� ��������� ���������
                                //function for checking the presence of directory(file+file size) in signed etalon directory (TO WRITE)

                                String checkSignedDir = config.signedPath + config.re + file.getName() + "." + file.length();
//                        //System.out.println(checkSignedDir);
                                File signedEtalonDir = new File(checkSignedDir);
                                String text = "\n������� ������ ���������� ����� \"" + file + "\" � ���������� \"" + fileListWith[i] + "\"\r\n";
//
                                appendUsingFileWriter(reportPath + config.re + config.AS + " �����(������� ������ ����������)" + formatForDateNow.format(dateNow) + ".txt", text);

                                if (signedEtalonDir.exists()) {


//                                config.fileToReplaceFromPaths.add(signedEtalonDir + config.re +
//                                        file.getName());
//                                config.fileToReplaceToPaths.add(fileListWith[i].toString());
//                                config.systemNameToReplace.add(config.AS);

                                    appendUsingFileWriter(config.fileListForReplace, config.AS+"\r\n");
                                    appendUsingFileWriter(config.fileListForReplace, signedEtalonDir + config.re +
                                            file.getName()+"\r\n");
                                    appendUsingFileWriter(config.fileListForReplace, fileListWith[i].toString()+"\r\n");
                                    appendUsingFileWriter(config.fileListForReplace, "n/a"+"\r\n");
                                    String renameBat = "rename \""+fileListWith[i].toString()+"\" \""+file.getName()+".backup\"";
                                    String copyBat = "copy \""+signedEtalonDir.getAbsolutePath()+config.re+file.getName()+"\" \""+fileListWith[i].toString()+"\"";
                                    appendUsingFileWriter(reportPath + config.re + config.AS + "_BatReplace.cmd.", renameBat+"\r\n");
                                    appendUsingFileWriter(reportPath + config.re + config.AS + "_BatReplace.cmd.", copyBat+"\r\n");

                                    String del = "del \""+fileListWith[i].toString()+"\"";
                                    String renameBatBckp = "rename \""+fileListWith[i].toString()+".backup\""+" \""+file.getName()+"\"";
                                    appendUsingFileWriter(reportPath + config.re + config.AS + "_BatBckp.cmd.", del+"\r\n");
                                    appendUsingFileWriter(reportPath + config.re + config.AS + "_BatBckp.cmd.", renameBatBckp+"\r\n");




//                                String fileListForReplace = config.mainClassPath + config.re + "fileListForReplace.txt";
//                                if (config.fileListForReplace == null)
//                                    config.fileListForReplace = fileListForReplace;

                                    if (config.replaceFile) {
                                        //function replace file on resource N: TO WRITE

//                                    fileListWith[i].renameTo(new java.io.File(fileListWith[i].getParent() + config.re
//                                            + "backup." + fileListWith[i].getName()));
//
//
//                                    //copy signed file to N: resource(TO WRITE)
//                                    File fileFromSignedEtalon = new java.io.File(signedEtalonDir.getPath() + config.re + fileListWith[i].getName());
//
//                                    try {
//                                        copyFileUsingStream(fileFromSignedEtalon, fileListWith[i], reportPath);
//                                    } catch (IOException e) {
//                                        e.getCause();
//                                        ;
//                                    }
//
                                        //write to fileLIstForReplase.xlsx paths of files


//                                    File fileListForReplacePath = new File(fileListForReplace);
//
//                                    if (fileListForReplacePath.exists())
//                                        System.out.println("EXIST " + fileListForReplace);

//                                    try {
//                                        List<String> resultFileFrom = readXLSXFile(fileListForReplace, true, 1, 2, "");
//                                        List<String> resultFileTo = readXLSXFile(fileListForReplace, true, 2, 2, "");
//
//                                        for (int count = 0; count < resultFileFrom.size(); count++) {
//                                            System.out.println(resultFileFrom.get(count));
//                                            System.out.println(resultFileTo.get(count));
//                                        }
//
//                                    } catch (Exception e) {
//                                        e.getCause();
//                                        JOptionPane.showConfirmDialog(null, e.getCause(), "Warning", JOptionPane.DEFAULT_OPTION);


//                                    File fileListForReplacePath = new File(fileListForReplace);
//
//                                    if (fileListForReplacePath.exists())
//                                        System.out.println("EXIST " + fileListForReplace);

//                                    try {
//                                        List<String> resultFileFrom = readXLSXFile(fileListForReplace, true, 1, 2, "");
//                                        List<String> resultFileTo = readXLSXFile(fileListForReplace, true, 2, 2, "");
//
//                                        for (int count = 0; count < resultFileFrom.size(); count++) {
//                                            System.out.println(resultFileFrom.get(count));
//                                            System.out.println(resultFileTo.get(count));
//                                        }
//
//                                    } catch (Exception e) {
//                                        e.getCause();
//                                    }


//                                    replaceFileFun(fileListWith[i], signedEtalonDir, reportPath);

                                    }
                                } else {
                                    //Function for creating packages for sign
                                    if (config.createPackages) {
                                        String allPackagesDirPath = reportPath + config.re + "������ �� ���������� �� " + formatForDateNow.format(dateNow);
                                        File allPackagesDir = new File(allPackagesDirPath);

                                        if (!allPackagesDir.exists())
                                            allPackagesDir.mkdir();

                                        String packageDirPath = allPackagesDir + config.re + file.getName() + "."
                                                + file.length();
                                        File packageDir = new File(packageDirPath);
                                        if (!packageDir.exists()) {
                                            packageDir.mkdir();
                                            File fileFrom = fileListWith[i];
                                            File fileTo = new File(packageDirPath + config.re + fileListWith[i].getName());
                                            try {
                                                copyFileUsingStream(fileFrom, fileTo, reportPath);
                                            } catch (IOException e) {
                                                e.getCause();
                                                JOptionPane.showConfirmDialog(null, e.getCause(), "Warning", JOptionPane.DEFAULT_OPTION);

                                            }


                                        } else {
                                            packageDir.mkdir();
                                            File fileFrom = fileListWith[i];
                                            File fileTo = new File(packageDirPath + config.re + fileListWith[i].getName());
                                            try {
                                                copyFileUsingStream(fileFrom, fileTo, reportPath);
                                            } catch (IOException e) {
                                                e.getCause();
                                                JOptionPane.showConfirmDialog(null, e.getCause(), "Warning", JOptionPane.DEFAULT_OPTION);

                                            }
                                        }

                                        String auxiliaryFilesPath = config.mainClassPath + config.re + "AuxiliaryFiles"; //���� ��������������� ������

                                        File fileFrom = new File(auxiliaryFilesPath + config.re + "description_clear.xlsx");
                                        File fileTo = new File(packageDirPath + config.re + "description.xlsx");
                                        try {
                                            copyFileUsingStream(fileFrom, fileTo, reportPath);
                                        } catch (IOException e) {
                                            e.getCause();
                                        }

//                                    try {
//                                        List<String> fileNameInList = readXLSXFile(auxiliaryFilesPath + config.re + "FullDescriptionFile.xlsx", false, 0, 8, "");
//                                        System.out.println(fileNameInList.toString());
//                                    } catch (Exception e) {
//                                        e.getCause();
//                                    }
//
                                    }

                                    text = "\n���������� ���������: " + file + "\r\n";

                                    if (!config.fileAnalyzed && config.justTakeReport) {
                                        appendUsingFileWriter(reportPath + config.re + config.AS + " �����(����� �� �������)" + formatForDateNow.format(dateNow) + ".txt", text);
                                        config.fileAnalyzed = true;
                                    }


                                }

                                existFile = true;
                            } else { // ���� ���� ��������� ������ �� �����
                                File backupFile = new java.io.File(fileListWith[i].getParent() + config.re + fileListWith[i].getName()+".backup");
//

                                if (!backupFile.exists()) {
                                    String text = "\n���� \"" + fileListWith[i] + "\"  �� ��������� ��������� �� � ����� ��������� ������ � ��������� � �������!\r\n";
//                                //System.out.println("\n���� \"" + file + "\" ������ � ���������� \"" + fileListWith[i] + "\" �� �� ��������� � ��������� ������!\r\n");
                                    //�������� ���� � ��������� ������� � �������������� ���������

                                    String pathForNewDir = file.getPath().substring(0, file.getPath().indexOf(file.getName())) + file.getName() + "." + fileListWith[i].length();

                                    File dir = new File(pathForNewDir);
                                    if (!dir.exists()) {
                                        if (dir.mkdir()) {
                                            File DEST_PATH = new java.io.File(pathForNewDir + config.re + file.getName());
//                                    //System.out.println("The folder: "+pathForNewDir+ " was created!");
                                            appendUsingFileWriter(reportPath + config.re + config.AS + " �����(��������� ����������)" + formatForDateNow.format(dateNow) + ".txt", text);
//                                    //System.out.println("copy the" + fileListWith[i] + " to " + DEST_PATH);

                                            try {
                                                copyFileUsingStream(fileListWith[i], DEST_PATH, reportPath);
                                            } catch (IOException e) {
                                                e.getCause();
                                            }
                                        }

                                        //Create package of file that was copied for signing TO WRITE
                                        if (config.createPackages) {
                                            String allPackagesDirPath = reportPath + config.re + "������ �� ���������� �� " + formatForDateNow.format(dateNow);
                                            File allPackagesDir = new File(allPackagesDirPath);

                                            if (!allPackagesDir.exists())
                                                allPackagesDir.mkdir();

                                            String packageDirPath = allPackagesDir + config.re + file.getName() + "."
                                                    + fileListWith[i].length();
                                            File packageDir = new File(packageDirPath);
                                            if (!packageDir.exists()) {
                                                packageDir.mkdir();
                                                File fileFrom = fileListWith[i];
                                                File fileTo = new File(packageDirPath + config.re + fileListWith[i].getName());
                                                try {
                                                    copyFileUsingStream(fileFrom, fileTo, reportPath);
                                                } catch (IOException e) {
                                                    e.getCause();
                                                    JOptionPane.showConfirmDialog(null, e.getCause(), "Warning", JOptionPane.DEFAULT_OPTION);

                                                }


                                            } else {
                                                packageDir.mkdir();
                                                File fileFrom = fileListWith[i];
                                                File fileTo = new File(packageDirPath + config.re + fileListWith[i].getName());
                                                try {
                                                    copyFileUsingStream(fileFrom, fileTo, reportPath);
                                                } catch (IOException e) {
                                                    e.getCause();
                                                }
                                            }

                                            String auxiliaryFilesPath = config.mainClassPath + config.re + "AuxiliaryFiles"; //���� ��������������� ������

                                            File fileFrom = new File(auxiliaryFilesPath + config.re + "description_clear.xlsx");
                                            File fileTo = new File(packageDirPath + config.re + "description.xlsx");
                                            try {
                                                copyFileUsingStream(fileFrom, fileTo, reportPath);
                                            } catch (IOException e) {
                                                e.getCause();
                                            }

//                                        try {
////                                        readXLSFile(auxiliaryFilesPath+config.re+"FullDescriptionFile.xls");
////                                        readXLSXFile(auxiliaryFilesPath + config.re + "FullDescriptionFile.xlsx", true, 0, 8, "");
//                                            List<String> fileNameInList = readXLSXFile(auxiliaryFilesPath + config.re + "FullDescriptionFile.xlsx", false, 0, 8, "");
//                                        } catch (Exception e) {
//                                            e.getCause();
//                                        }

                                        }


                                    }
//                            else{
//                                //System.out.println("The Path: "+pathForNewDir+ " Exist!");
//                            }

                                    existFile = true;
                                }
                            }
                        }



                    }


                }
            }
        }

        return existFile;

    }

    public void appendUsingFileWriter(String filePath, String text) {
        File file = new File(filePath);
        FileWriter fr = null;
        try {
            fr = new FileWriter(file, true);
            fr.write(text);

        } catch (IOException e) {
            e.getCause();
            JOptionPane.showConfirmDialog(null, e.getCause(), "Warning", JOptionPane.DEFAULT_OPTION);

            ;
        } finally {
            try {
                fr.close();
            } catch (IOException e) {
                e.getCause();
                JOptionPane.showConfirmDialog(null, e.getCause(), "Warning", JOptionPane.DEFAULT_OPTION);

                ;
            }
        }
    }

    public void copyFileUsingStream(File source, File dest, String reportPath) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }

        if (config.justTakeReport) {
            String text = "\n���������� ���������: " + dest + "\r\n";
            appendUsingFileWriter(reportPath + config.re + config.AS + " �����(����� �� �������)" + formatForDateNow.format(dateNow) + ".txt", text);
        }

    }

    public void removeOldReports(String reportPath) {
        File REPORT1 = new java.io.File(reportPath + config.re + config.AS + " �����(����� �� �������)" + formatForDateNow.format(dateNow) + ".txt");
        File REPORT2 = new java.io.File(reportPath + config.re + config.AS + " �����(���������� �� �������)" + formatForDateNow.format(dateNow) + ".txt");
        File REPORT3 = new java.io.File(reportPath + config.re + config.AS + " �����(������� ������ ����������)" + formatForDateNow.format(dateNow) + ".txt");
        File REPORT4 = new java.io.File(reportPath + config.re + config.AS + " �����(��������� ����������)" + formatForDateNow.format(dateNow) + ".txt");
        if (REPORT1.exists()) {
            REPORT1.delete();
        }
        if (REPORT2.exists()) {
            REPORT2.delete();
        }
        if (REPORT3.exists()) {
            REPORT3.delete();
        }
        if (REPORT4.exists()) {
            REPORT4.delete();
        }

    }

    public static void readXLSFile(String xlsFilePath) throws IOException {
        InputStream ExcelFileToRead = new FileInputStream(xlsFilePath);
        HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow row;
        HSSFCell cell;

        Iterator rows = sheet.rowIterator();

        while (rows.hasNext()) {
            row = (HSSFRow) rows.next();
            Iterator cells = row.cellIterator();

            while (cells.hasNext()) {
                cell = (HSSFCell) cells.next();

                if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                    System.out.print(cell.getStringCellValue() + " ");
                } else if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                    System.out.print(cell.getNumericCellValue() + " ");
                } else {
                    //U Can Handel Boolean, Formula, Errors
                }
            }
            //System.out.println();
        }

    }


    public static List<String> readXLSXFile(String filePath, boolean readColumn, int columnNumber, int startRowReadingNumber, String cellLocation) throws IOException {
        List<String> result = new ArrayList();
        JOptionPane.showConfirmDialog(null, "lyalechkaReadSTART", "Warning", JOptionPane.DEFAULT_OPTION);
        JOptionPane.showConfirmDialog(null, "lyalechkaRead-Path: "+filePath, "Warning", JOptionPane.DEFAULT_OPTION);

        try {
            InputStream ExcelFileToRead = new FileInputStream(filePath);
            JOptionPane.showConfirmDialog(null, "lyalechkaRead-Check", "Warning", JOptionPane.DEFAULT_OPTION);


            XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);//no reference

            XSSFWorkbook test = new XSSFWorkbook();

            XSSFSheet sheet = wb.getSheetAt(0);
            XSSFRow row;
            XSSFCell cell;
            Iterator rows = sheet.rowIterator();

            int i = 0;
            while (i < startRowReadingNumber - 2) {
                if (rows.hasNext()) {
                    rows.next();
                    i++;
                }
            }

            if (readColumn) {

                while (rows.hasNext()) {
                    row = (XSSFRow) rows.next();
                    Iterator cells = row.cellIterator();

                    cell = (XSSFCell) cells.next();


                    result.add(row.getCell(columnNumber).toString());
//                    System.out.println(row.getCell(columnNumber));
                }

            } else {
                row = (XSSFRow) rows.next();

                result.add(row.getCell(columnNumber).toString());

            }

        } catch (Exception e) {
            e.getCause();
            JOptionPane.showConfirmDialog(null, e.getCause(), "Warning", JOptionPane.DEFAULT_OPTION);

        }
        JOptionPane.showConfirmDialog(null, "lyalechkaReadFinish", "Warning", JOptionPane.DEFAULT_OPTION);


        return result;


    }

    public void writeXLSXReplaceFile(String filePath, List<String> system, List<String> filefrom,
                                     List<String> fileTo, String status) throws IOException {


        try {
            String excelFileName = filePath;//name of excel file

            String sheetName = "Sheet1";//name of sheet

            XSSFWorkbook wb = new XSSFWorkbook();
            XSSFSheet sheet = wb.createSheet(sheetName);

        XSSFRow row = sheet.createRow(0);

        //iterating c number of columns
        XSSFCell cell1 = row.createCell(0);
        XSSFCell cell2 = row.createCell(1);
        XSSFCell cell3 = row.createCell(2);
        XSSFCell cell4 = row.createCell(3);


        cell1.setCellValue("��");
        cell2.setCellValue("����������� ���� ��� ������");
        cell3.setCellValue("���������� ���� �� ������� N:\\\\");
        cell4.setCellValue("������ ������");


        //iterating r number of rows
        for (int r = 0; r < filefrom.size(); r++) {
            row = sheet.createRow(r + 1);

            //iterating c number of columns
            cell1 = row.createCell(0);
            cell2 = row.createCell(1);
            cell3 = row.createCell(2);
            cell4 = row.createCell(3);

            cell1.setCellValue(system.get(r));
            cell2.setCellValue(filefrom.get(r));
            cell3.setCellValue(fileTo.get(r));
            cell4.setCellValue(status);

        }
        }catch (Exception e) {
            e.getCause();
            JOptionPane.showConfirmDialog(null, e.getCause(), "Warning", JOptionPane.DEFAULT_OPTION);

        }
    }


    public void replaceFileFun(File replaceableFile, File signedEtalonDir, String reportPath) {

        replaceableFile.renameTo(new java.io.File(replaceableFile.getParent() + config.re
                + replaceableFile.getName()+".backup"));


        //copy signed file to N: resource(TO WRITE)
        File fileFromSignedEtalon = new java.io.File(signedEtalonDir.getPath() + config.re + replaceableFile.getName());

        try {
            copyFileUsingStream(fileFromSignedEtalon, replaceableFile, reportPath);
        } catch (IOException e) {
            e.getCause();

            ;
        }
    }

}
